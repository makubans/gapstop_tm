import numpy as np
import pytest

from gapstop.storage import TileQueue
from gapstop.storage import TileStore

# mockup mpi communicator
class _MockComm:
    def Get_rank(self):
        return 0
    def Barrier(self):
        pass

_comm = _MockComm()

def test_queue(tmp_path):
    name = tmp_path / "foo.queue"
    q = TileQueue(name, 15, _comm)

    res = []

    # get some batches from the queue
    qp = q.process(2)
    for i in range(4):
        res.append(next(qp))
    next(qp) # an item is only labelled finished after control is back from yield

    # finish all items from the queue
    q = TileQueue(name, 15, _comm, overwrite=False)
    for i in q.process(3):
        res.append(i)

    ref = [[14, 13], [12, 11], [10, 9], [8, 7], [6, 5, 4], [3, 2, 1], [0]]

    assert ref == res

def test_storage(tmp_path):

    store = TileStore(tmp_path / "maps")
    foo   = store.create_dataset("foo")

    a = np.random.randn(12,3)
    b = np.random.randn(12,3)

    foo[0] = a
    foo[1] = b

    assert store.size() == 2
    assert foo.size()   == 2

    _a = foo[0]
    _b = foo[1]

    assert np.all(a == _a)
    assert np.all(b == _b)

    with pytest.raises(IndexError):
        foo[3]

    bar = store.create_dataset("bar")
    with pytest.raises(AssertionError):
        store.size()
