import numpy as np
from scipy.spatial.transform import Rotation as R
from scipy.ndimage import map_coordinates

from gapstop.rotation import rotate_volume, rotate_image

# swap coordinates to index correctly according to mrc's standard cs
cs_swap    = [2,1,0]

def test_rot3():
    """Test 3d rotations by euler angles."""

    # test volume
    vol = np.ones((40,40,40), dtype=np.float32)
    vol[8:-8,12:-12,10:-10] = 1
    # probe position: in mrc's standard cs this is at (10,12,8)!
    probe = np.array([8,12,10])
    vol[tuple(probe)] = 2
    
    rot_vecs = [
        [0,0,np.pi/2],
        [0,np.pi/2,0],
        [np.pi/2,0,0],
        [0,np.pi/2,np.pi/2],
        [0,np.pi/2,np.pi/2],
        [np.pi/2,np.pi/2,0],
        [np.pi/2,0,np.pi/2],
        [np.pi/2,np.pi/2,np.pi/2],
    ]

    # stopgap's idea of 'center'
    center = (np.floor(np.array(vol.shape)) / 2)

    # the swap to correct coords is invariant for this shape, but it wouldn't
    # for unsymmetric shapes!
    center = center[cs_swap]

    for rv in rot_vecs:
        rot    = R.from_rotvec(rv)
        angles = rot.as_euler('zxz', degrees=True)
        rvol   = rotate_volume(vol, angles)

        rprobe = (rot.apply(probe[cs_swap]-center) + center)[cs_swap]
        rval = map_coordinates(rvol, rprobe[np.newaxis,:].T)
        # check whether rotation is accurate on 1px scale
        assert rval > 1.0

def test_rot2():
    """Validate rot2 against rot3."""
    img = np.zeros((72,96), dtype=np.float32)
    img[24:48,32:64] = 1.0

    for a in [10,30,45,90,130,180]:
        rimg = rotate_image(img, a)
        rvol = rotate_volume(img[np.newaxis], np.array([0,0,a]))

        assert np.allclose(rimg, rvol[0])

if __name__ == "__main__":
    test_rot3()
        
