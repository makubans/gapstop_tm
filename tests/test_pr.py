import numpy as np

from gapstop.filter.bandpass import generate_bpf
from gapstop.pr import init_phase_randomization


def test_pr():
    p = {"noise_corr": True,
         "lp_rad": 30.,
         "hp_rad": 3.,
         "lp_sigma": 1.,
         "hp_sigma": 1,
    }
    bpf  = generate_bpf((72,72), p)
    tmpl = np.random.randn(72,72)
    tmpl_bpf = bpf[0]

    pr_tmpl = init_phase_randomization(tmpl, tmpl_bpf, p)
    assert pr_tmpl.ndim == 2

    p["noise_corr"] = False
    pr_tmpl = init_phase_randomization(tmpl, tmpl_bpf, p)
    assert pr_tmpl is None

    #TODO: add some more tests here?
