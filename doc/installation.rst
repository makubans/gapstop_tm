.. _installation:

============
Installation
============

GAPSTOP™ can be installled with pip:

.. code-block:: Bash

   pip install "gapstop @ git+https://gitlab.mpcdf.mpg.de/bturo/gapstop_tm.git"

Dependencies
^^^^^^^^^^^^

Beside some python-only dependencies that can be handled automatically by pip,
gapstop explicitly depends on:

* MPI. In case a working MPI implementation is available during installation,
  ``mpi4py`` will be installed automatically. It might be necessary to set the
  environment variable ``MPICC`` to point to the ``mpicc`` compiler wrapper.
  Please refer to the `official documentation
  <https://mpi4py.readthedocs.io/en/stable/install.html>`_ for details.

* GAPSTOP depends on ``jax`` and ``jaxlib``. To use a GPU enabled version, please refer to the
  `jax documentation <https://jax.readthedocs.io/en/latest>`_ for
  information on the installation of jaxlib with GPU support on your system.
  A CPU-only version of gapstop can be readily installed by specifying the optional ``[cpu]`` dependency, e.g.:

  .. code-block:: Bash

     pip install "gapstop[cpu] @ git+https://gitlab.mpcdf.mpg.de/bturo/gapstop_tm.git"

  