.. _tutorials:

.. toctree::
   :maxdepth: 2
   :hidden:

   Dataset <dataset>
   Inputs preparation <inputs>
   Running GAPSTOP™ <run_gapstop>
   Results evaluation <results_eval>


Tutorial
========


`Dataset <dataset.html>`_
-------------------------
The section describes the content of the tutorial dataset.


`Inputs preparation <inputs.html>`_
-----------------------------------
The section shows how to prepare some of the necessary inputs using the `cryoCAT <https://github.com/turonova/cryocat>`_
package.


`Running GAPSTOP™ <run_gapstop.html>`_
-----------------------------------------
The section describes how to run `gapstop`.


`Results evaluation <results_eval.html>`_
-----------------------------------------
The section describes how to evaluate the results and create a particle list from the scores maps.
