.. gapstop documentation master file, created by
   sphinx-quickstart on Fri Nov 10 11:07:02 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

:html_theme.sidebar_secondary.remove:

.. toctree::
   :maxdepth: 1
   :hidden:

   User Guide <user_manual/index>
   Tutorial <tutorials/index>
   Installation <installation>


********************
**GAPSTOP**:sup:`TM` 
********************

GPU-Accelerated Python STOPgap for Template Matching
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Date**: |today| **Version**: |version|

**Useful links**:
`Source Repository <https://gitlab.mpcdf.mpg.de/bturo/gapstop_tm>`__ |
`Issues & Ideas <https://gitlab.mpcdf.mpg.de/bturo/gapstop_tm/-/issues>`__ |
`cryoCAT <https://cryocat.readthedocs.io/latest/>`__ |
`STOPGAP <https://github.com/wan-lab-vanderbilt/STOPGAP>`__ |


**GAPSTOP**:sup:`TM` is able to leverage the power of GPU accelerated multi-node HPC
systems to be efficiently used for template matching. It speeds up template
matching by using an MPI-parallel layout and offloading the compute-heavy
correlation kernel to one or more accelerator devices per MPI-process
using `jax <https://github.com/google/jax>`_.

The template matching in **GAPSTOP**:sup:`TM` is algorithmically based on 
`STOPGAP <https://github.com/wan-lab-vanderbilt/STOPGAP>`_ developed by W. Wan's lab. For more details please read
the original `STOPGAP publication <https://doi.org/10.1107/S205979832400295X>`_. To fully
exploit the capabilities of the software we recommend to read the manuscript parameters optimization by
`Leon-Cruz et al. <https://link.springer.com/article/10.1038/s41467-024-47839-8>`_ and to perform in silico template
analysis in `cryoCAT <https://cryocat.readthedocs.io/latest/tutorials/peak_analysis/peak_analysis.html>`_.

Citation
--------
To cite **GAPSTOP**:sup:`TM` please refer to original STOPGAP manuscript by `Wan et al.
<https://doi.org/10.1107/S205979832400295X>`_ as well as to 
`Leon-Cruz et al. <https://link.springer.com/article/10.1038/s41467-024-47839-8>`_.


.. grid:: 1 1 3 3
    :gutter: 3
    :padding: 2 2 0 0
    :class-container: sd-text-center

    .. grid-item-card:: User guide
        :img-top: _static/gapstop_user_guide.png
        :class-card: intro-card
        :shadow: md

        The user guide provides in-depth information on the
        key concepts of cryoCAT with useful background information and explanation.

        +++

        .. button-ref:: usage
            :ref-type: ref
            :click-parent:
            :color: secondary
            :expand:

            To the user guide
    
    .. grid-item-card::  Tutorial
        :img-top: _static/gapstop_tutorial.png
        :class-card: intro-card
        :shadow: md

        The tutorial shows all steps necessary to prepare inputs, run GAPSTOP™ and analyze the results.

        +++
    
        .. button-ref:: tutorials
            :ref-type: ref
            :click-parent:
            :color: secondary
            :expand:

            To the tutorial

    .. grid-item-card::  Installation
        :img-top: _static/gapstop_install.png
        :class-card: intro-card
        :shadow: md

        The installation guide provides information on how to isntall cryoCAT.

        +++

        .. button-ref:: installation
            :ref-type: ref
            :click-parent:
            :color: secondary
            :expand:

            To the installation

