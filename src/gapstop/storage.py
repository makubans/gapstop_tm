"""Storage for gapstop."""
import os
import pathlib
import json

import numpy as np
import fasteners

class TileQueue:
    """Simple tile management queue for parallel processesing."""
    def __init__(self, name, n, comm, overwrite=True):
        self.name = pathlib.Path(name)
        self.lock = fasteners.InterProcessLock(
            self.name.with_suffix(".lock")
        )
        if comm.Get_rank() == 0:
            if overwrite:
                queue = {
                    "pending": int("1"*n, 2),
                    "finished": 0,
                    "n_tiles": n,
                }
                with open(self.name, "w") as fp:
                    json.dump(queue, fp)
            else:
                # get unfinished items
                with open(self.name, "r") as fp:
                    queue = json.load(fp)
                n = queue["n_tiles"]
                queue["pending"] = int("1"*n, 2) - queue["finished"]
                with open(self.name, "w") as fp:
                    json.dump(queue, fp)
        comm.Barrier()

    def _pop(self,n):
        t = []
        with self.lock:
            with open(self.name, "r") as fp:
                queue = json.load(fp)

            q  = queue["pending"]
            ti = q.bit_length() - 1
            for i in range(n):
                ti = q.bit_length() - 1
                if ti < 0:
                    break
                q -= (1 << ti)
                t.append(ti)

            queue["pending"] = q
            with open(self.name, "w") as fp:
                json.dump(queue, fp)

        return t

    def _finish(self, t):
        if not isinstance(t, list):
            t = [t]
        with self.lock:
            with open(self.name, "r") as fp:
                queue = json.load(fp)
                for ti in t:
                    queue["finished"] += (1<<ti)
            with open(self.name, "w") as fp:
                json.dump(queue, fp)

    def process(self, n):
        t = self._pop(n)
        while len(t) != 0:
            yield t
            self._finish(t)
            t = self._pop(n)

    def n_tiles(self):
        with open(self.name, "r") as fp:
            return json.load(fp)["n_tiles"]

    def status(self):
        with open(self.name, "r") as fp:
            queue = json.load(fp)
        queue["pending"]  = queue["pending"].bit_count()
        queue["finished"] = queue["finished"].bit_count()
        return queue
            
class TiledDataset:
    """Dataset that is stored as tiles in the filesystem.

    Such Dataset rely on tiles being processed by single processes
    only. So no guards on parallel access are implemented here!
    """

    def __init__(self, name):
        """Initialize dataset."""
        self.name = pathlib.Path(name)
        self.name.mkdir(exist_ok=True, parents=True)

    def __setitem__(self, i, tile):
        """Write tile i."""
        np.savez(self.name / f"{i:d}", tile)

    def __getitem__(self, i):
        """Get tile i."""
        tname = self.name / f"{i:d}.npz"
        if not tname.exists():
            raise IndexError(f"tile {i} not in the dataset.")
        return np.load(tname)["arr_0"]

    def size(self):
        return len([n for n in os.listdir(self.name) if n.endswith(".npz")])

    def remove(self):
        for f in os.listdir(self.name):
            os.remove(self.name / f)
        self.name.rmdir()

class TileStore:
    """Storage of tiled maps."""

    def __init__(self, root):
        """Initialize store under root."""
        self.name      = pathlib.Path(root)
        self.datasets  = {}

        self.name.mkdir(parents=True, exist_ok=True)

    def __getitem__(self, k):
        return self.datasets[k]
                  
    def create_dataset(self, name):
        """Append a dataset to the store."""
        if name in self.datasets:
            raise ValueError(f"Dataset {name} already exists in store.")
        td = TiledDataset(self.name / name)
        self.datasets[name] = td
        return td

    def size(self):
        if len(self.datasets) == 0:
            return 0
        l = [i.size() for i in self.datasets.values()]
        err = "Inconsistent number of tiles stored in datasets."
        assert len(set(l)) == 1, err
        return l[0]

    def remove(self):
        for d in self.datasets.values():
            d.remove()
        self.name.rmdir()
