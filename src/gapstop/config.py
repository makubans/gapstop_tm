"""Handle parameters and settings."""
import pathlib

import numpy as np
import pandas as pd
import starfile

from . import cryocat_geom as geom

_params_fields = {
    'rootdir':         str,
    'outputdir':       str,
    'vol_ext':         str,
    'tomo_name':       str,
    'tomo_num':        int,
    'tomo_mask_name':  str,
    'wedgelist_name':  str,                  
    'tmpl_name':       str,
    'mask_name':       str,
    'symmetry':        str,
    'anglist_name':    str,
    'anglist_order':   str,
    'angincr':         float,
    'angiter':         float,
    'phi_angincr':     float,
    'phi_angiter':     float,
    'tilelist_name':   str,
    'smap_name':       str,
    'omap_name':       str,
    'tmap_name':       str,
    'lp_rad':          float,
    'lp_sigma':        float,
    'hp_rad':          float,
    'hp_sigma':        float,
    'binning':         int,
    'calc_exp':        bool,
    'calc_ctf':        bool,
    'apply_laplacian': bool,
    'noise_corr':      bool,
    'fourier_crop':    bool,
    'scoring_fcn':     str,
    'write_raw':       bool,
    'tiling':          str,
}

# defaults for params
_defaults = {
    'outputdir':       'outputs/',
    'vol_ext':         '.mrc',
    'fourier_crop':    True,
    'scoring_fcn':     'flcf',
    'write_raw':       False,
    'tiling':          "legacy_fix",
    'smap_name':       "scores",
    'omap_name':       "angles",
    'tmap_name':       "noise",
    'lp_sigma':        3.0,
    'hp_sigma':        2.0,
    'calc_exp':        True,
    'calc_ctf':        True,
    'apply_laplacian': False,
    'noise_corr':      True,
    'anglist_order':  'zxz',
}

# helpers to aggregate on wedgelist
def _unique(x):
    u = x.unique()
    if not len(u) == 1:
        raise ValueError(f"wedgelist item {x.name} must be unique.")
    return u[0]

def _array(x):
    #TODO: it would be nicer to have the array columns as real arrays
    return list(x)

# in addition to the type info this also has aggregation info.
_wedgelist_fields = {
    'tomo_num':      (int, _unique),
    'pixelsize':     (float, _unique),
    'tomo_x':        (int, _unique), 
    'tomo_y':        (int, _unique), 
    'tomo_z':        (int, _unique), 
    'z_shift':       (float, _unique), 
    'tilt_angle':    (float, _array),
    'defocus':       (float, _array),
    'defocus1':      (float, _array),
    'defocus2':      (float, _array),
    'astig_ang':     (float, _array),
    'pshift':        (float, _array),
    'exposure':      (float, _array),
    'voltage':       (float, _unique),
    'amp_contrast':  (float, _unique),
    'cs':            (float, _unique),
}

def _anchor_paths(params):
    """Anchor paths either absolute or relative to rootdir."""
    path_vars = [
        'outputdir',
        'tomo_name',
        'tomo_mask_name',
        'wedgelist_name',
        'tmpl_name',
        'mask_name',
        'anglist_name',
        'tilelist_name',
    ]

    # first resolve root itself (in case relative)
    _resolve = lambda x: str(pathlib.Path(x).resolve())
    params["rootdir"] = params["rootdir"].apply(_resolve)

    def _root_anchor(root, path):
        p = pathlib.Path(path)
        r = pathlib.Path(root)
        if not p.is_absolute():
            p = r / p
        return str(p.resolve())

    eff_path_vars = params.columns.intersection(path_vars)
    for pv in eff_path_vars:
        params[pv] = params.apply(
            lambda x: _root_anchor(x.rootdir, x[pv]), axis=1
        )

    return params

def write_config(outfile, minimal=False):
    """Write default parameters."""
    if minimal:
        keys = _params_fields.keys() - _defaults.keys()
        keys = [k for k in _params_fields.keys() if not k in _defaults.keys()]
        params = {k: _params_fields[k] for k in keys}
    else:
        params = _params_fields | _defaults
    params = pd.DataFrame({k: [v] for k,v in params.items()})
    params = params.map(lambda x: str(x).replace(" ", "_"))
    starfile.write(params, outfile, sep=" ")

def read_params(param_file):
    """Read parameters from star-file."""

    params = starfile.read(param_file)

    types  = {c: _params_fields[c] for c in params.columns}
    params = params.astype(types)

    # update with defaults in case and drop nan's
    params = pd.DataFrame(params, columns=_params_fields.keys())
    params = params.fillna(_defaults)
    params = params.dropna(axis=1)

    # anchor path-like parameters
    params = _anchor_paths(params)

    return params

def read_wedgelist(wedgelist_file):
    """Read wedgelist information."""

    wl = starfile.read(wedgelist_file, always_dict=True)

    if not 'stopgap_wedgelist' in wl:
        raise KeyError("wedgelist must contain block 'stopgap_wedgelist'")
    wl = wl['stopgap_wedgelist']

    types  = {c: _wedgelist_fields[c][0] for c in wl.columns}
    wl = wl.astype(types)

    if not len(wl.pixelsize.unique()) == 1:
        msg = "wedgelist: 'pixelsize' must be unique but is not."
        raise RuntimeError(msg)

    #TODO: in the original code 'defocus1', 'defocus2' and 'astig_angle' are
    # plugged together to replace 'defocus' but not further used. So throw an
    # error here for the moment in case
    if all([c in wl.columns for c in ['defocus1', 'defocus2', 'astig_angle']]):
        msg = "wedgelist contains 'defocus' AND ('defocus1', 'defocus2', " + \
              "'astig_angle'). This case is currently not handled."
        raise RuntimeError(msg)

    wl_groups = wl.groupby("tomo_num")
    agg       = {key: _wedgelist_fields[key][1] for key in wl.columns}

    #TODO: add some print out (after some verbosity level is introduced)

    return wl_groups.agg(agg).drop(labels="tomo_num", axis=1)

def gen_angles(
    angincr,
    angiter,
    phi_angincr,
    phi_angiter,
    symmetry,
    order,
    outroot = None,
):
    """Wrap .cryocat_geom with output to be used directly from cli."""
    if not order in ('zxz', 'zzx'):
        msg = "Unknown 'order' parameter; Possible values are 'zxz' or 'zzx'";
        raise ValueError(msg)
    angles = geom.generate_angles(
        angincr,
        angiter,
        phi_angincr,
        phi_angiter,
        symmetry,
        True if order == 'zzx' else False,
    )

    if outroot is None:
        outroot = pathlib.Path(".")

    outname = f"angles_{angincr}_{angiter}_{phi_angincr}_{phi_angiter}.txt"
    outfile = outroot / outname

    with outfile.open("w") as fp:
        np.savetxt(fp, angles, fmt="%.5e", delimiter=",")
 
    print(f"\nGenerated angles in {outfile}")

    return angles

def read_angles(params):
    """Read or generate angles information."""
    anglist = params.get("anglist_name")
    order   = params["anglist_order"]
    if not anglist is None:
        if order == 'zxz':
            cols = [0,1,2]
        elif order == 'zzx':
            cols = [0,2,1]
        else:
            msg = "Unknown '_anglist_order'; Possible values are 'zxz' or 'zzx'";
            raise ValueError(msg)
        angles = np.loadtxt(anglist, delimiter=",", usecols=cols)
        print(f"\nReading angles from {anglist}")
    else:
        angincr     = params["angincr"]
        angiter     = params.get("angiter")
        phi_angincr = params.get("phi_angincr")
        phi_angiter = params.get("phi_angiter")
        symmetry    = params.get("symmetry")
        outroot     = pathlib.Path(params["rootdir"])
        angles      = gen_angles(
            angincr,
            angiter,
            phi_angincr,
            phi_angiter,
            symmetry,
            order,
            outroot,
        )
        if order == 'zzx':
            angles = angles[:,[0,2,1]]

    return angles
