"""Read tile/mask from mrcfile."""
import warnings
import numpy as np
import mrcfile

def read_tm_tiles(fname, c, tilesize, tile_idx):
    """Read tile or mask with idx 'tile_idx' as given by 'ftype'.

    Parameters
    ----------    
    fname: str of Path
        name of mrc file to load from
    c: dict-like
        extraction coordinates (stopgaps's 'o["c"]')
    tilesize:
        size of the tile to read data into. Must be consistent with
        'c'. Both can, e.g., be generated from `.dd.compute_tiles`
    tile_idx: integer
        index of tile to extract
    legacy_tiles: bool
        Compatibility with legacy tiling logic

    Returns
    -------
    out: numpy.ndarray
        Array with the tomogram tile or mask.
    """
    # indices for extraction and pasting
    extract_start = c["es"][tile_idx].astype("int")
    extract_end   = c["ee"][tile_idx].astype("int")
    paste_start   = c["ts"][tile_idx].astype("int")
    paste_end     = c["te"][tile_idx].astype("int")

    with mrcfile.mmap(fname) as mrc:
        tile = np.ones(tilesize, dtype=np.float32) * mrc.header.dmean
        paste = tuple([slice(paste_start[i], paste_end[i]) for i in range(3)])
        extract = tuple([slice(extract_start[i], extract_end[i]) for i in range(3)])
        tile[paste] = mrc.data[extract]

    return tile
