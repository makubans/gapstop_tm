"""Wedgemask slices."""
import numpy as np

from ..rotation import rotate_image

def _crop(arr, cropsize, center=None):
    """Crop array around center."""
    if center is None:
        center = np.array(arr.shape)//2

    cropsize = np.array(cropsize)

    start = center - cropsize//2
    end   = start + cropsize
    idx   = tuple(slice(s,e) for s,e in zip(start,end))

    return arr[idx]

def generate_wedgemask_slices(wedgelist, tmpl_bpf, tile_bpf):
    """Generate wedgemask slices."""

    # for internal use this is always valid
    assert len(np.unique(tmpl_bpf.shape)) == 1

    tmplsize = tmpl_bpf.shape[0]
    tilesize = np.array(tile_bpf.shape)

    # original codes uses matlab axes 1 and 3 which correspond to x and z
    # according to the original gsg_mrcread; so with mrcfile this is 2 and 0
    mx  = np.max(tilesize[[2,0]])
    img = np.zeros((mx, mx))
    img[:,mx//2] = 1.

    bpf_idx = tmpl_bpf > 0

    # tmpl filter stuff
    f_slice_idx    = []
    f_slice_weight = np.zeros_like(tmpl_bpf)
    weight         = np.zeros_like(tmpl_bpf)

    # tile filter stuff
    tile_bin_slice = np.zeros(tilesize[[2,0]], dtype="float32")

    for alpha in wedgelist["tilt_angle"]:
    
        # the original code here uses tom_rotate.c which interprets 2d-arrays
        # as (x,y), i.e., a positive rotation is clockwise. `rotate_image`
        # interprets 2d arrays as (y,x) and the angle has thus to be inverted
        # to get consistent behavior.
        r_img = rotate_image(img, -alpha, extrapolate='none')
        
        # template filter
        crop_r_img = _crop(r_img, (tmplsize, tmplsize)) > np.exp(-2)

        slice_vol = np.fft.ifftshift(
            np.transpose(np.tile(crop_r_img, (tmplsize,1,1)), (2,0,1))
        )
        
        slice_idx = slice_vol & bpf_idx
        weight   += slice_idx
        f_slice_idx.append(np.nonzero(slice_idx))

        # tile filter
        r_img   = np.fft.fftshift(np.fft.fft2(r_img))
        new_img = np.real(
            np.fft.ifft2(np.fft.ifftshift(_crop(r_img, tilesize[[2,0]])))
        )
        new_img /= np.max(new_img)

        tile_bin_slice += new_img > np.exp(-2)
        

    # generate tile binary wedge filter
    tile_bin_slice = (tile_bin_slice > 0).astype("float32")
    f_tile_bin_wedge = np.fft.ifftshift(
        np.transpose(np.tile(tile_bin_slice, (tilesize[1],1,1)), (2,0,1))
    )

    # invert values for filter
    w_idx = np.nonzero(weight)
    f_slice_weight[w_idx] = 1. / weight[w_idx]

    f_bin_wedge = np.zeros_like(weight)
    f_bin_wedge[w_idx] = 1.0

    return f_slice_idx, f_slice_weight, f_bin_wedge, f_tile_bin_wedge
