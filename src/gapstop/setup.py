"""Setting up stopgap template matching."""
import pathlib

from .config import read_wedgelist, read_angles
from .prepare_template import get_template_and_mask
from .dd import compute_tiles
from .filter import generate_filters, generate_bpf
from .pr import init_phase_randomization
from .util import tomo_size_from_mrc


def setup(params, n_tiles=None, random_seed=None):
    """Setup data structures to run a single instance of template matching.
    
    Parameters
    ----------
    params: pandas.Series or dict-like
        a single row from the parameters starfile.
    n_tiles: int
        number of tiles the tomogram is decomposed into.
    random_seed: int (optional)
        random seed used for deterministic simulation setup during testing.
    """
    # read config files
    wedgelist = read_wedgelist(params["wedgelist_name"])
    angles    = read_angles(params)

    pixelsize = (wedgelist.loc[params["tomo_num"]]["pixelsize"]
                 * params["binning"])

    tmpl, mask = get_template_and_mask(params)

    c, tilesize = compute_tiles(params, n_tiles, tmpl)

    tmpl_bpf, tile_bpf = generate_bpf([tmpl.shape, tilesize], params)

    pr_tmpl = init_phase_randomization(tmpl, tmpl_bpf, params, random_seed)

    tmpl_filt, tile_filt = generate_filters(
        tmpl_bpf,
        tile_bpf,
        wedgelist,
        params,
    )

    out = {
        "angles": angles,
        "pixelsize": pixelsize,
        "tmpl": tmpl,
        "mask": mask,
        "c": c,
        "tilesize": tilesize,
        "pr_tmpl": pr_tmpl,
        "tmpl_filt": tmpl_filt,
        "tile_filt": tile_filt,
        "tomo_size": tomo_size_from_mrc(params["tomo_name"]),
    }

    return out
