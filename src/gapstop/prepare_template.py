"""Prepare template."""
import warnings
import pathlib

import numpy as np
from scipy.spatial.transform import Rotation as R

from .util import read_volume
from .del2 import del2
from .rotation import rotate_volume

def _sum_rotations(vol, angles, center=None, binary=False):
    sum_vol = np.zeros_like(vol)
    for alpha in angles:
        if binary:
            sum_vol += rotate_volume(vol, alpha, center) >= 0.9
        else:
            sum_vol += rotate_volume(vol, alpha, center)
    return sum_vol

def _symmetrize_volume(
    vol,
    symmetry,
    init_euler=None,
    center=None,
    reweight=True,
    binary=False,
):
    """Symmetrize volume with given symmetry."""

    # TODO: decide and clean-up
    msg1 = "kwarg['binary'] is ignored in consistency with matlab source."
    msg2 = "kwarg['reweight'] changes implementation details but seems to "\
           "have the same effect?"
    warnings.warn(msg1, category=RuntimeWarning)
    warnings.warn(msg2, category=RuntimeWarning)

    symmetry = symmetry.upper()

    if symmetry == "C1":
        return vol

    sym_group = R.create_group(symmetry, axis="Z")

    if not init_euler is None:
        sym_group *= R.from_euler("zxz", init_euler, degrees=True)

    angles = sym_group.as_euler("zxz", degrees=True)

    sym_vol = _sum_rotations(vol, angles, center, binary)

    if reweight:
        weights = _sum_rotations(np.ones_like(vol), angles, center)
        sym_vol = sym_vol / weights
    else:
        sym_vol = sym_vol / len(angles)

    return sym_vol

def get_template_and_mask(conf):
    """Read template and mask."""

    tmpl = read_volume(conf["tmpl_name"])
    mask = read_volume(conf["mask_name"])

    if not len(np.unique(tmpl.shape)) == 1:
        msg = "template must have equal dimensions; instead is {tmpl.shape}"
        raise ValueError(msg)

    if not tmpl.shape == mask.shape:
        msg = "template and mask shape mismatch: " \
              f"template has shape: {tmpl.shape}, " \
              f"mask has shape:     {mask.shape}"
        raise ValueError(msg)

    # preprocess template
    tmpl = _symmetrize_volume(tmpl, conf["symmetry"])
    if conf["apply_laplacian"]:
        tmpl = del2(tmpl)

    return tmpl, mask
