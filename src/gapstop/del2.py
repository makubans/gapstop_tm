"""Port of matlab's del2."""

from functools import partial
import warnings

import jax.numpy as jnp
from jax import jit

msg = "gapstop.del2 implements matlab's del2 behaviour of scaling the " + \
      "result by 1/4 for ndim <= 2 and by 1/(2*ndim) for ndim > 2."


@partial(jit, static_argnames=["axis", "mode"])
def _central_diff(a, axis, mode="valid"):
    """2nd order central difference along 'axis'.

    Parameters
    ----------    

    a: array-like
        Array to compute differences on.
    axis: int
        Axis/dimension in which to compute the differences.
    mode: string, optional
        One of ["same"] (default "valid"). "valid" indicates to compute
        differences only on the valid part of the array, i.e., effectively
        reducing the output-shape in dimension 'axis' by 2. "same" performs
        a linear extrapolation of the values on the boundary.

    Returns
    -------
    out: jax.DeviceArray
        Array with the 2nd order central differences computed on a.
    """
    if axis >= a.ndim:
        raise ValueError("number of dimensions of 'a' smaller than 'axis'")
    fwd_slices       = [slice(None)] * a.ndim
    bac_slices       = [slice(None)] * a.ndim
    fwd_slices[axis] = slice(1,None)
    bac_slices[axis] = slice(None,-1)
    fwd_slices       = tuple(fwd_slices)
    bac_slices       = tuple(bac_slices)
    # apply forward difference
    out = jnp.diff(a[fwd_slices], axis=axis)
    # apply 'backward difference'
    out = out - jnp.diff(a[bac_slices], axis=axis)
    if mode == "same":
        # extrapolate linearly
        slices       = [slice(None)] * a.ndim
        ## prepend values
        slices[axis] = [0,1]
        prep_slices  = tuple(slices)
        dprep = jnp.diff(out[prep_slices], axis=axis)
        ## append values
        slices[axis] = [-2,-1]
        app_slices  = tuple(slices)
        dapp = jnp.diff(out[app_slices], axis=axis)
        ## plug together
        slices[axis] = [0]
        start = tuple(slices)
        slices[axis] = [-1]
        end = tuple(slices)
        out = jnp.concatenate(
            (out[start] - dprep, out, out[end] + dapp),
            axis=axis
        )
    return out

def del2(x):
    """Application of Laplace operator to x.

    This is reverse engineered from and equivalent with Matlab's del2.
    Values on the boundary are interpolated from the partial derivatives in
    each axis direction. For arrays with number of dimensions <= 2, this
    function returns the action of the Laplace operator scaled by 1/4; for
    arrays with number of dimensions > 2 instead, the result is scaled by
    1/(2N) with N being the number of dimensions of the input.

    Parameters
    ----------
    x: array-like
        Input array to apply the Laplace operator to.

    Returns
    -------
    out: jax.DeviceArray
        Result of the action of the scaled Laplace operator on x.
    """
    warnings.warn(msg, category=RuntimeWarning)

    out = jnp.zeros(x.shape)
    ndim = out.ndim
    for i in range(ndim):
        out += _central_diff(x, i, mode="same")
    return out/4 if ndim < 3 else out/(2*ndim)
